
window.onload = function () {

	var i = 0;
	var table = document.getElementById('table');
	var startTime = new Date().getTime();
	var currencyList = [
		{"path": "currency.AUD.json"},
		{"path": "currency.USD.json"},
		{"path": "currency.GBP.json"},
		{"path": "currency.ARS.json"},
		{"path": "currency.BRL.json"},
		{"path": "currency.CAD.json"},
		{"path": "currency.CLP.json"},
		{"path": "currency.CNY.json"},
		{"path": "currency.HRK.json"},
		{"path": "currency.CZK.json"},
		{"path": "currency.DKK.json"},
		{"path": "currency.EUR.json"},
		{"path": "currency.HKD.json"},
		{"path": "currency.HUF.json"},
		{"path": "currency.INR.json"},
		{"path": "currency.IDR.json"},
		{"path": "currency.ILS.json"},
		{"path": "currency.JPY.json"},
		{"path": "currency.KWD.json"},
		{"path": "currency.MYR.json"},
		{"path": "currency.MXN.json"},
		{"path": "currency.XPF.json"},
		{"path": "currency.NZD.json"},
		{"path": "currency.NOK.json"},
		{"path": "currency.OMR.json"},
		{"path": "currency.PLN.json"},
		{"path": "currency.QAR.json"},
		{"path": "currency.SAR.json"},
		{"path": "currency.SGD.json"},
		{"path": "currency.SBD.json"},
		{"path": "currency.ZAR.json"},
		{"path": "currency.KRW.json"},
		{"path": "currency.LKR.json"},
		{"path": "currency.SEK.json"},
		{"path": "currency.CHF.json"},
		{"path": "currency.TWD.json"},
		{"path": "currency.THB.json"},
		{"path": "currency.TRY.json"},
		{"path": "currency.AED.json"},
		{"path": "currency.VUV.json"},
		{"path": "currency.VND.json"},
		{"path": "currency.WST.json"}
	];

	function init () {

		var nextFile = getNextFile(currencyList[i].path);

		if(nextFile){

			populateTable(nextFile);

			i++;

			init();

		}

	}

	function getNextFile (filePath) {

		var xmlhttp = new XMLHttpRequest(), response;

		xmlhttp.onreadystatechange = function () {

			if (xmlhttp.readyState == 4) {

				if (xmlhttp.status == 200) {

					response = JSON.parse(xmlhttp.responseText);

				}
				else {

					response = false;

				}

			}

		};

		xmlhttp.open("GET", "assets/" + filePath + '?cachebuster=' + startTime, false);
		xmlhttp.send();

		return response;

	}

	function populateTable (nextFile) {

		for (var j = 0; j < nextFile.length; j++) {

			var row = '<tr></tr>';

			row.innerHTML = '<td>' + nextFile[i].currencyName + '</td>' + '<td>' + nextFile[i].currencyTitle + '</td>' + '<td>' + nextFile[i].bbCashTChqs + '</td>' + '<td>' + nextFile[i].bbForeignCheques + '</td>' + '<td>' + nextFile[i].bbImt + '</td>' + '<td>' + nextFile[i].bsCashTmcTChqs + '</td>' + '<td>' + nextFile[i].bsImt + '</td>';

			table.appendChild(row);

		}

	}

	init();

};