# Commbank Technical Frontend Test
Welcome to commonwealthbank technical frontend test. The purpose of this assignment is to test your familiarity with JavaScript, HTML, DOM manipulation, browser performance and layout.

## Background
The code that you are given contains a naive implementation of ajax loader that fetches 42 json files. After the file is retrieved from the server it is loaded into a table.

## Task
# There are 2 objectives in this test.
* The first objective is to develop the header and footer as per the image header-footer.png supplied, required assets can be found in the assets folder.
* The second objective is to modify the JavaScript code to get the loader working and to optimise the code.
* You are not allowed to remove `cachebust` parameter from the URL in the javascript file. The solution may rely on browser caching but only within one page load.
* The page has to stay interactive (i.e. you can scroll) at all times.
